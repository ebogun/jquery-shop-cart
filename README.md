# jQuery Shop Cart

jQuery Shop Cart

## Getting Started
Download the [production version][min] or the [development version][max].

[min]: https://raw.github.com//grunt/master/dist/jquery.shop-cart.min.js
[max]: https://raw.github.com//grunt/master/dist/jquery.shop-cart.js

In your web page:

```html
<script src="jquery.js"></script>
<script src="dist/jquery.shop-cart.min.js"></script>
<script>
jQuery(function($) {
  $.awesome(); // "awesome"
});
</script>
```

## Documentation
_(Coming soon)_

## Examples
_(Coming soon)_

## Release History
_(Nothing yet)_
