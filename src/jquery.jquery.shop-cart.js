/*
 * jquery.shop-cart
 * https://bitbucket.org/ebogun/jquery-shop-cart
 *
 * Copyright (c) 2015 Eugene Bogun
 * Licensed under the MIT license.
 */

(function($) {

  // Collection method.
  $.fn.jquery_shop_cart = function() {
    return this.each(function(i) {
      // Do something awesome to each selected element.
      $(this).html('awesome' + i);
    });
  };

  // Static method.
  $.jquery_shop_cart = function(options) {
    // Override default options with passed-in options.
    options = $.extend({}, $.jquery_shop_cart.options, options);
    // Return something awesome.
    return 'awesome' + options.punctuation;
  };

  // Static method default options.
  $.jquery_shop_cart.options = {
    punctuation: '.'
  };

  // Custom selector.
  $.expr[':'].jquery_shop_cart = function(elem) {
    // Is this element awesome?
    return $(elem).text().indexOf('awesome') !== -1;
  };

}(jQuery));
